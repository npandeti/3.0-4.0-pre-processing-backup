
/*
 * Copyright (C) 2016 Motorola Solutions, Inc. All rights reserved.
 *
 * $Id: E911SchemaUpdater.java 76955 2018-02-09 22:58:28Z rreeves $
 */

package com.clj2.business.util;

import com.clj2.business.config.ConfigPrep;
import com.clj2.business.config.E911Config;
import com.clj2.business.config.E911ConfigMigrator;
import com.clj2.business.config.E911Properties;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.dellroad.stuff.main.MainClass;
import org.dellroad.stuff.pobj.PersistentObject;
import org.dellroad.stuff.pobj.PersistentObjectDelegate;
import org.dellroad.stuff.spring.SpringSQLSchemaUpdater;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class E911SchemaUpdater extends MainClass {

    protected ClassPathXmlApplicationContext context;

    private void openContext() {
        String path = this.getClass().getSimpleName() + ".xml";
        this.log.info("opening application context " + path);
        this.context = new ClassPathXmlApplicationContext(path, getClass());
    }

    private void closeContext() {
        this.log.info("closing application context");
        this.context.close();
        this.context = null;
    }

    @Override
    public int run(String[] args) throws Exception {
        try {
            List<String> arguments = Arrays.asList(args); // for quick lookup of optional args

            // must have between one and five parameters
            if (args.length > 0 && args.length < 6) {
                System.setProperty("e911.db.jdbcUrl", args[0]);
                System.setProperty("e911.schema.updater.url", args[0]);
                if (args.length > 1) {
                    System.setProperty("e911.config.default.file", args[1]);
                    System.setProperty("e911.config.file", args[1]);
                }

                // pull db credentials from the url
                String[] tokens = System.getProperty("e911.db.jdbcUrl").split("&|\\?");
                for (String token : tokens) {
                    if (token.startsWith("user")) {
                        String[] usernameToken = token.split("=");
                        if (usernameToken.length > 1) {
                            System.setProperty("e911.db.user", usernameToken[1]);
                        }
                    }
                    if (token.startsWith("password")) {
                        String[] passwordToken = token.split("=");
                        if (passwordToken.length > 1) {
                            System.setProperty("e911.db.pass", passwordToken[1]);
                        }
                    }
                }
            } else {
                this.usageError();
                return 1;
            }

            // Open context
            this.openContext();

            // Get DataSource and SchemaUpdater
            DataSource dataSource = this.context.getBean(DataSource.class);
            SpringSQLSchemaUpdater schemaUpdater = this.context.getBean(SpringSQLSchemaUpdater.class);

            // Apply update(s)
            if (!arguments.contains("skip-db-upgrade")) {
                this.log.info("Initializing and updating database");
                schemaUpdater.initializeAndUpdateDatabase(dataSource);
            }

            // run config migration (if requested)
            if (args.length > 1) {
                if (arguments.contains("prep-3.3")) {
                    // transform config.xml and make it compliant with 4.0 (JiBX) code
                    final FileSystemResource configXML =
                            new FileSystemResource(E911Properties.getRequiredProperty("e911.config.file"));
                    final FileSystemResource defaultXML =
                            new FileSystemResource(E911Properties.getRequiredProperty("e911.config.default.file"));
                    this.log.info("Prepping config.xml: " + defaultXML.getURL());
                    ConfigPrep configPrep = new ConfigPrep(defaultXML, null);
                    configPrep.preMigrationPrep();

                    PersistentObjectDelegate<E911Config> schemaDelegate = E911Config.SCHEMA_PBOJ_DELEGATE;
                    E911ConfigMigrator.getInstance().migrateIfNecessary(defaultXML, configXML, schemaDelegate);

                    E911Config.getInstance();
                } else {
                    // loading this class initializes and updates the XML schema
                    E911Config config = E911Config.getInstance();

                    // for 4.x, serialize the newly upgraded config, both with/without prepended updates list (pobj updates)
                    final FileSystemResource defaultXML =
                            new FileSystemResource(E911Properties.getRequiredProperty("e911.config.default.file"));
                    PersistentObject.write(config, config.getPersistentObjectDelegate(false), defaultXML.getFile());
                    String withUpdatesFilename = args[1] + "-with-pobjupdates.xml";
                    PersistentObject.write(config, config.getPersistentObjectDelegate(true),
                            new FileOutputStream(withUpdatesFilename));
                }
            }

            if (arguments.contains("pre-process")) {
                PreProcessingEngine preProcessingEngine = new PreProcessingEngine();
                preProcessingEngine.dumpTables(dataSource);

                // create the bundle based on the pwd folder name, presuming it will contain the specific site naming
                File currentDirectory = new File(new File(".").getCanonicalPath());
                File bundleFile = new File(currentDirectory.getName() + "-pre-processing-bundle.zip");
                preProcessingEngine.createBundle(bundleFile);
            }
            // Done
            this.closeContext();
            return 0;
        } catch (Exception e) {
            this.log.error("e911SchemaUpdater had an ERROR: ", e);
            return 1;
        }
    }

    @Override
    protected void usageMessage() {
        System.err.println("Usage: cfgvalidate database-url [config-xml-path] [prep-3.3] [pre-process] [skip-db-upgrade]");
    }

    public static void main(String[] args) {
        new E911SchemaUpdater().doMain(args);
    }
}

