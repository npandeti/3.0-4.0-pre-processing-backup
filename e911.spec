# $Id: e911.spec 91243 2019-06-17 19:30:03Z jgonzalez $

#
# Required defines:
#
#   base_name
#   full_version
#   config_pkg
#

%{?_with_official: %define is_official 1}%{!?_with_official: %define is_official 0}

%if 0%{?el7}
%define __jar_repack %{nil}
%endif

%define name        %{base_name}

%define logdir      %{_localstatedir}/log
%define baselogdir  %{logdir}/%{base_name}
%define pkgdir      %{_datadir}/%{name}
%define confdir     /shared/lib/%{base_name}
%if 0%{?el7}
%define webappdir   /var/tomcat/webapps
%define sysddir     %{_prefix}/lib/systemd/system
%else
%define webappdir   /srv/tomcat/webapps
%endif
%define statedir    %{_var}/lib/%{base_name}
%define e911config  %{confdir}/config.xml
%define pluginsdir  /usr/lib/nagios/plugins
%define initdir     %{_sysconfdir}/init.d
%define wardir      %{webappdir}/%{warname}
%define warname     ROOT
%define tomcatuser  tomcat
%define tomcatgroup tomcat
%define ecatsdir    %{_localstatedir}/ecats

# cfgsys defines
%define cfgsysname  %{base_name}-cfgsys
%define cfgsysdir   %{pkgdir}/cfgsys
%define servicexml  %{pkgdir}/service.xml
%define jsvc        %{_sbindir}/jsvc
%define cfgsyspidf  %{_var}/run/%{cfgsysname}.pid
%define cfgsysmain  %{config_pkg}.sys.Main
%define cfgsyslog   %{baselogdir}/cfgsys.log

Name:               %{name}
Version:            %{full_version}
Release:            1
Summary:            Emergency CallWorx Backend Java Web Application
Group:              CallWorx
License:            Commercial
Distribution:       Emergency CallWorx
Vendor:             %{distribution}
BuildRoot:          %{_tmppath}/%{name}-root
Buildarch:          noarch
URL:                http://www.emergencycallworx.com/
Packager:           %{is_official}
BuildRequires:      unzip
BuildRequires:      ecx-fonts
Requires:           e911-apache = 1.0.90048
Requires:           e911-tomcat = 1.0.89558
Requires:           e911-ups = 1.0.82145
Requires:           e911-pdu = 1.0.82133
Requires:           e911-dev = 1.0.82118
Requires:           e911-fonts = 1.0.82180
Requires:           e911-syslog = 1.0.89396
Requires:           e911-mysql-federated
Requires:           e911-java = 1.0.82126
Requires:           htmldoc
Requires:           libreadline-java
Requires:           logwarn >= 1.0.4
Requires:           jsvc >= 1.0.7
%if 0%{?el7}
Requires:           aliproxyservice = 1.0.91242
%endif

%description
%{summary}.

%clean
rm -rf ${RPM_BUILD_ROOT}

%prep

%build

# Substitution function
subst()
{
    sed -r \
      -e 's|@basename@|%{base_name}|g' \
      -e 's|@bindir@|%{_bindir}|g' \
      -e 's|@cfgsysdir@|%{cfgsysdir}|g' \
      -e 's|@cfgsysmain@|%{cfgsysmain}|g' \
      -e 's|@cfgsysname@|%{cfgsysname}|g' \
      -e 's|@cfgsyspidf@|%{cfgsyspidf}|g' \
      -e 's|@cfgsyslog@|%{cfgsyslog}|g' \
      -e 's|@datadir@|%{_datadir}|g' \
      -e 's|@e911config@|%{e911config}|g' \
      -e 's|@jsvc@|%{jsvc}|g' \
      -e 's|@pkgdir@|%{pkgdir}|g' \
      -e 's|@pluginsdir@|%{pluginsdir}|g' \
      -e 's|@servicexml@|%{servicexml}|g' \
      -e 's|@wardir@|%{wardir}|g' \
      -e 's|@warname@|%{warname}|g' \
      -e 's|@webappdir@|%{webappdir}|g'
}

# Generate scripts
for FILE in `find %{_sourcedir}/scripts -name "*.*"` ; do
    DEST=`echo "${FILE}" | sed -rn 's|^%{_sourcedir}/(.+)(\.[^.]*)$|\1|gp'`
    if [ -z "${DEST}" ]; then
        echo "can't parse filename \`${FILE}'" 1>&2
        exit 1
    fi
    mkdir -p `dirname "${DEST}"`
    subst < "${FILE}" > `dirname "${DEST}"`/`basename "${DEST}"`
done

%install

# Install exploded WAR file
install -d ${RPM_BUILD_ROOT}%{wardir}
unzip -q -d ${RPM_BUILD_ROOT}%{wardir} %{_sourcedir}/%{name}-%{full_version}.war
chmod -R a+rx,u+w ${RPM_BUILD_ROOT}%{wardir}
find ${RPM_BUILD_ROOT}%{wardir} -type f -print0 | xargs -0 chmod 644

# Install init.d scripts
install -d ${RPM_BUILD_ROOT}%{initdir}
for FILE in `find scripts -type f -name "*.init"`; do
    SVCNAME=`echo "${FILE}" | sed -rn 's|^scripts/(.*)\.init$|\1|gp'`
    install "${FILE}" ${RPM_BUILD_ROOT}%{initdir}/"${SVCNAME}"
    rm -f "${FILE}"
done

%if 0%{?el7}
# Install systemd unit file(s)
install -d ${RPM_BUILD_ROOT}%{sysddir}
install %{_sourcedir}/systemd/* ${RPM_BUILD_ROOT}%{sysddir}
%endif

# Install command line scripts
install -d ${RPM_BUILD_ROOT}%{_bindir}
for FILE in `find scripts -type f \! -name "*.init"`; do
    install "${FILE}" ${RPM_BUILD_ROOT}%{_bindir}/
done

# Install e911-cfgsys files
install -d ${RPM_BUILD_ROOT}/%{cfgsysdir}/base
cp -a %{_sourcedir}/config ${RPM_BUILD_ROOT}/%{cfgsysdir}/base/
for TYPE in normal verbose daemon; do
    install -d ${RPM_BUILD_ROOT}/%{cfgsysdir}/${TYPE}
    install %{_topdir}/SOURCES/conf/log4j-cfgsys-${TYPE}.xml ${RPM_BUILD_ROOT}/%{cfgsysdir}/"${TYPE}"/log4j2.xml
done

# RTDB related files - rtdb(1) script logging config and init script, schema files
install -d ${RPM_BUILD_ROOT}%{pkgdir}/rtdb
install %{_sourcedir}/conf/log4j-rtdb.xml ${RPM_BUILD_ROOT}%{pkgdir}/rtdb/log4j2.xml
install %{_sourcedir}/conf/rtdb.init ${RPM_BUILD_ROOT}%{pkgdir}/rtdb/
for FILE in `find %{_sourcedir}/schema/old-rtdb -name "version*.xml"`; do
    install "${FILE}" ${RPM_BUILD_ROOT}%{pkgdir}/rtdb/
done
install %{_sourcedir}/schema/rtdb.xml ${RPM_BUILD_ROOT}%{pkgdir}/rtdb/version.%{full_version}.xml
ln -s version.%{full_version}.xml ${RPM_BUILD_ROOT}%{pkgdir}/rtdb/current.xml

# Support files
install -d ${RPM_BUILD_ROOT}%{pkgdir}/alitest
install %{_sourcedir}/conf/log4j-alitest.xml ${RPM_BUILD_ROOT}%{pkgdir}/alitest/log4j2.xml
install -d ${RPM_BUILD_ROOT}%{pkgdir}/call-tracker
install %{_sourcedir}/conf/log4j-call-tracker.xml ${RPM_BUILD_ROOT}%{pkgdir}/call-tracker/log4j2.xml
install -d ${RPM_BUILD_ROOT}%{pkgdir}/cfgvalidate
install %{_sourcedir}/conf/log4j-cfgvalidate.xml ${RPM_BUILD_ROOT}%{pkgdir}/cfgvalidate/log4j2.xml
install -d ${RPM_BUILD_ROOT}%{pkgdir}/xsl
install %{_sourcedir}/config/xsl/util.xsl ${RPM_BUILD_ROOT}%{pkgdir}/xsl/
install -d ${RPM_BUILD_ROOT}%{pkgdir}/pductl
install %{_sourcedir}/xsl/pductl-list.xsl ${RPM_BUILD_ROOT}%{pkgdir}/pductl/list.xsl
install %{_sourcedir}/xsl/pductl-outlets.xsl ${RPM_BUILD_ROOT}%{pkgdir}/pductl/outlets.xsl
install -d ${RPM_BUILD_ROOT}%{pkgdir}/upsctl
install %{_sourcedir}/xsl/upsctl-list.xsl ${RPM_BUILD_ROOT}%{pkgdir}/upsctl/list.xsl
install %{_sourcedir}/xsl/upsctl-outlets.xsl ${RPM_BUILD_ROOT}%{pkgdir}/upsctl/outlets.xsl
install -d ${RPM_BUILD_ROOT}%{pkgdir}/devctl
install %{_sourcedir}/xsl/devctl-list.xsl ${RPM_BUILD_ROOT}%{pkgdir}/devctl/list.xsl
install %{_sourcedir}/xsl/devctl-commands.xsl ${RPM_BUILD_ROOT}%{pkgdir}/devctl/commands.xsl
install %{_sourcedir}/xml/devices.xml ${RPM_BUILD_ROOT}%{pkgdir}/devctl/
install -d ${RPM_BUILD_ROOT}%{pkgdir}/msrp4j
install %{_sourcedir}/msrp4j-cli.jar ${RPM_BUILD_ROOT}%{pkgdir}/msrp4j/

# Create log directory
install -d ${RPM_BUILD_ROOT}%{baselogdir}

# Create ecats directory
install -d ${RPM_BUILD_ROOT}%{ecatsdir}

# Read-write state dir
install -d ${RPM_BUILD_ROOT}%{statedir}
# e911-utils libraries
install -d ${RPM_BUILD_ROOT}%{_datadir}/%{name}-utils
unzip -q -d ${RPM_BUILD_ROOT}%{_datadir}/%{name}-utils %{_topdir}/SOURCES/%{name}-util-libs.zip

%post
if [ ! -e %{cfgsyslog} ]; then
    touch %{cfgsyslog}
    chmod 644 %{cfgsyslog}
fi

# javamelody disabled by default
melody-ctl disable

%package utils
Summary:                ECX %{name} utilities
Group:                  System/Setup

%description utils
ECX %{name} utilities.

%files
%defattr(644,%{tomcatuser},%{tomcatgroup},755)
%dir %{baselogdir}
%dir %{ecatsdir}
%{statedir}
%{pkgdir}
%{webappdir}/%{warname}
%defattr(644,root,root,755)
%attr(755,root,root) %{_bindir}/alitest
%attr(755,root,root) %{_bindir}/rtdb
%attr(755,root,root) %{_bindir}/melody-ctl
%attr(755,root,root) %{_bindir}/call-tracker
%attr(755,root,root) %{_bindir}/cfgvalidate
%attr(755,root,root) %{_bindir}/msrp4j
%attr(755,root,root) %{_bindir}/e911-cfgsys
%attr(755,root,root) %{_bindir}/hung-calls
%attr(755,root,root) %{initdir}/*
%if 0%{?el7}
%attr(644,root,root) %{sysddir}/e911-cfgsys.service
%endif

%files utils
%defattr(644,root,root,755)
%{_datadir}/%{name}-utils
%attr(755,root,root) %{_bindir}/e911-schema-update
%attr(755,root,root) %{_bindir}/import-pre-processed-mysql-dump
%attr(755,root,root) %{_bindir}/retrieve-voicemail-password-from-customer
