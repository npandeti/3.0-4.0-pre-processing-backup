
/*
 * Copyright (C) 2017 Motorola Solutions, Inc. All rights reserved.
 *
 * $Id: ConfigPrep.java 66626 2017-03-06 17:51:55Z rreeves $
 */

package com.clj2.business.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * Performs a series of transformations to config.xml before and after migration to 4.x.
 */
public class ConfigPrep {

    private static final Path TMP_DIR = Paths.get(System.getProperty("java.io.tmpdir"));

    private Resource configXML;
    private Resource serviceXML;

    private Transformer restoreServiceTransformer;

    public ConfigPrep(Resource serviceXML, Resource configXML) throws IOException, TransformerConfigurationException {
        this.configXML = configXML;
        this.serviceXML = serviceXML;

        restoreServiceTransformer =
          initializeXSLT("com/clj2/business/util/copyOriginalService.xslt", "defaultXML", serviceXML.getURI().getPath());
    }

    /**
     * Applies a dozen or so structure changes to this object's <i>service.xml</i> to make it compliant with this newer, 4.x
     * codebase. This must be done prior to 3.3 -> 4.x config migration.
     *
     * @throws IOException
     * @throws TransformerException
     */
    public void preMigrationPrep() throws IOException, TransformerException {
        // get a source of the passed file
        Source xmlInput = new StreamSource(Paths.get(serviceXML.getURI()).toFile());

        // create a temp config.xml file to use as the resulting transformation
        Path tmpFile = Files.createTempFile(TMP_DIR, "prepped-3.3-to-4.0-config.xml", null);
        Result xmlOutput = new StreamResult(tmpFile.toFile());

        Transformer transformer = initializeXSLT("com/clj2/business/util/prepConfigForMigration.xslt", null, null);
        transformer.transform(xmlInput, xmlOutput);

        // lastly we need to move the tmp file to the working config.xml
        Files.move(tmpFile, Paths.get(serviceXML.getURI()), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Runs the migrated config through a series of modifications: <ol> <li>Restores the E911Service section of the original (but
     * prepped) config.xml</li> <li>Re-applies a few migrated entities to the E911Service section</li> <li>Finalizes all the above
     * changes to the actual, working config.xml path</li> </ol>
     *
     * @param migratedConfigXML
     * @throws IOException
     * @throws TransformerException
     */
    public void postMigrationAssembly(Resource migratedConfigXML) throws IOException, TransformerException {

        // Stage 1: the original E911Service section contains valuable comments, so we restore it here
        Source xmlInput = new StreamSource(Paths.get(migratedConfigXML.getURI()).toFile());
        Path restoredServiceConfigXML = Files.createTempFile(TMP_DIR, "restored-service-config.xml", null);
        Result xmlOutput = new StreamResult(restoredServiceConfigXML.toFile());

        restoreServiceTransformer.transform(xmlInput, xmlOutput);

        // Stage 2: although we just restored the entire service section, there are portions with migrated info we want to keep
        Transformer copyMigratedTrunksTransformer =
          initializeXSLT("com/clj2/business/util/copyBackServiceElements.xslt", "migratedConfigXML",
            migratedConfigXML.getURI().getPath());

        xmlInput = new StreamSource(Paths.get(restoredServiceConfigXML.toUri()).toFile());
        Path updatedTrunksConfigXML = Files.createTempFile(TMP_DIR, "final-config.xml", null);
        Result updatedTrunksOutput = new StreamResult(updatedTrunksConfigXML.toFile());
        copyMigratedTrunksTransformer.transform(xmlInput, updatedTrunksOutput);

        // Stage 3: we need to move the tmp file to the working config.xml
        formatXML(updatedTrunksConfigXML.toFile());
        Files.copy(updatedTrunksConfigXML, Paths.get(configXML.getURI().getPath()), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Helper method for producing transformer objects based on XSLTs on the classpath.
     *
     * @param xsltPath
     * @param referenceXmlParam
     * @param referencesXmlPath
     * @return
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    private Transformer initializeXSLT(String xsltPath, String referenceXmlParam, String referencesXmlPath)
      throws TransformerConfigurationException, IOException {
        File xsltFile = new ClassPathResource(xsltPath).getFile();
        Source xslt = new StreamSource(xsltFile);
        Transformer result = TransformerFactory.newInstance().newTransformer(xslt);
        if (referenceXmlParam != null) {
            result.setParameter(referenceXmlParam, referencesXmlPath);
        }

        return result;
    }

    /**
     * Quick "pretty print" formatting of the passed file.
     *
     * @param xmlFile
     */
    public void formatXML(File xmlFile) {
        try {
            SAXReader reader = new SAXReader();
            Document doc = reader.read(xmlFile.toURI().toURL());
            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setIndent(true);
            format.setIndentSize(4);

            FileOutputStream fileOutputStream = new FileOutputStream(xmlFile);
            XMLWriter xw = new XMLWriter(fileOutputStream, format);
            xw.write(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException, TransformerException {
        FileSystemResource configXML = new FileSystemResource("/home/rreeves/development/migration/comments-test/config.xml");
        FileSystemResource serviceXML = new FileSystemResource("/home/rreeves/development/migration/comments-test/service.xml");
        FileSystemResource migratedXML = new FileSystemResource("/home/rreeves/development/migration/comments-test/migrated.xml");

        ConfigPrep configPrep = new ConfigPrep(serviceXML, configXML);
        configPrep.preMigrationPrep();
        configPrep.postMigrationAssembly(migratedXML);
    }
}
