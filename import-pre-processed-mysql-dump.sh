#!/bin/bash


# Set constants and defaults
NAME="import-pre-processed-mysql-dump.sh"
LOGFILE_PATH=/var/log/e911/
LOGFILE=${LOGFILE_PATH}updates.log

# Usage message
usage()
{
    echo "Usage:" 1>&2
    echo "    ${NAME}" 1>&2
    echo "    -h    Display this help message" 1>&2
}

# Log functions
log()
{
    echo $(date "+%Y-%m-%d %T,%3N") ${NAME}: ${1+"$@"} | tee -a ${LOGFILE} 1>&2
}

# Error function
errout()
{
    log ERROR: ${1+"$@"}
    exit 1
}

# Bail on errors
set -e

#make sure logfile exists
mkdir -p $LOGFILE_PATH
>> ${LOGFILE}

# Get sed flag
case `uname -s` in
    Darwin|*BSD)
        SEDFLAG='-E';
        ;;
    *)
        SEDFLAG='-r';
        ;;
esac

# Parse flags passed in on the command line
while [ ${#} -gt 0 ]; do
    case "$1" in
        -h|--help)
            usage
            exit
            ;;
        --)
            shift
            break
            ;;
        -*)
            echo "${NAME}: unrecognized flag \`${1}'" 1>&2
            usage
            exit 1
            ;;
        *)
            break
            ;;
    esac
done

if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-tables.sql ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-tables.sql not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-Line.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-Line.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-Incident.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-Incident.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-IncidentType.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-IncidentType.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-IncidentPriority.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-IncidentPriority.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-IncidentStatus.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-IncidentStatus.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-IncidentSubtype.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-IncidentSubtype.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-IncidentDisposition.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-IncidentDisposition.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-Agency.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-Agency.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-UnitStatus.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-UnitStatus.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-Unit.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-Unit.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-RunGroup.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-RunGroup.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-RequestProvider.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-RequestProvider.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-RequestCategory.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-RequestCategory.bak not found! Not loading tables."
fi
if [ ! -f /usr/share/e911/upgrade/e911-pre-processed-table-Event.bak ]; then
    errout "File /usr/share/e911/upgrade/e911-pre-processed-table-Event.bak not found! Not loading tables."
fi

log "Loading tables..."

mysql -ue911 -pe911 -De911 < /usr/share/e911/upgrade/e911-pre-processed-tables.sql
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_Line (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-Line.bak' INTO TABLE PRE_PROCESSED_TABLE_Line;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_Incident (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-Incident.bak' INTO TABLE PRE_PROCESSED_TABLE_Incident;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_IncidentType (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-IncidentType.bak' INTO TABLE PRE_PROCESSED_TABLE_IncidentType;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_IncidentPriority (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-IncidentPriority.bak' INTO TABLE PRE_PROCESSED_TABLE_IncidentPriority;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_IncidentStatus (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-IncidentStatus.bak' INTO TABLE PRE_PROCESSED_TABLE_IncidentStatus;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_IncidentSubtype (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-IncidentSubtype.bak' INTO TABLE PRE_PROCESSED_TABLE_IncidentSubtype;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_IncidentDisposition (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-IncidentDisposition.bak' INTO TABLE PRE_PROCESSED_TABLE_IncidentDisposition;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_Agency (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-Agency.bak' INTO TABLE PRE_PROCESSED_TABLE_Agency;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_UnitStatus (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-UnitStatus.bak' INTO TABLE PRE_PROCESSED_TABLE_UnitStatus;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_Unit (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-Unit.bak' INTO TABLE PRE_PROCESSED_TABLE_Unit;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_RunGroup (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-RunGroup.bak' INTO TABLE PRE_PROCESSED_TABLE_RunGroup;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_RequestProvider (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-RequestProvider.bak' INTO TABLE PRE_PROCESSED_TABLE_RequestProvider;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_RequestCategory (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-RequestCategory.bak' INTO TABLE PRE_PROCESSED_TABLE_RequestCategory;"
mysql -ue911 -pe911 -De911 -e "CREATE TABLE PRE_PROCESSED_TABLE_Event (id bigint(20) NOT NULL, uuid VARCHAR(36) NOT NULL);"
mysql -uroot -De911 -e "LOAD DATA INFILE '/usr/share/e911/upgrade/e911-pre-processed-table-Event.bak' INTO TABLE PRE_PROCESSED_TABLE_Event;"

log "Tables loaded."

log "Calculating counts..."
mysql -E -ue911 -pe911 -De911 -e 'select (select count(*) from CallHistory) as CallHistory,  (select count(*) from CallHistory_DispatchGroup) as CallHistory_DispatchGroup, (select count(*) from CallDiscrepancy2) as CallDiscrepancy2, (select count(*) from Participant2) as Participant2,  (select count(*) from Message2) as Message2, (select count(*) from PRE_PROCESSED_TABLE_Line) as PRE_PROCESSED_TABLE_Line' | tee -a ${LOGFILE} 2>&1

#now set the premissions so that the application, running as the tomcat user, can write to the updates.log fle
chown tomcat:tomcat ${LOGFILE}
