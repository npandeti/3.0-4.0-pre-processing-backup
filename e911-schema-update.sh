#!/bin/sh
# $Id: e911-schema-update.sh 75263 2017-11-30 23:45:22Z shammock $

# Set constants
NAME="@basename@-schema-update"
LIBDIR="@datadir@/@basename@-utils/lib"
CLASSDIR="@datadir@/@basename@-utils/classes"
MAINCLASS="com.clj2.business.util.E911SchemaUpdater"
URL_PARAMS="jdbcCompliantTruncation=false&cachePrepStmts=true&prepStmtCacheSize=200&prepStmtCacheSqlLimit=4096&cacheCallableStmts=true&cacheResultSetMetadata=true&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&includeInnodbStatusInDeadlockExceptions=true&includeThreadDumpInDeadlockExceptions=true&cacheServerConfiguration=true"

# Set defaults
DB_HOST="localhost:3306"
DB_DATABASE="unknown"
DB_USER="root"
DB_PASS=""
CONFIG_XML=""
PREP_3_3=""
PRE_PROCESS=""
SKIP_DB=""

# Usage message
usage()
{
    echo "Usage:" 1>&2
    echo "    ${NAME} [-h host] [-u user] [-p password] [-c config-xml] [--prep-3.3] [--pre-process] [--skip-db] database" 1>&2
    echo "Options:" 1>&2
    echo "    -h             Specify MySQL host (default \`${DB_HOST}')" 1>&2
    echo "    -u             Specify MySQL user (default \`${DB_USER}')" 1>&2
    echo "    -p             Specify MySQL password (default none)" 1>&2
    echo "    -c             Specify config.xml (default none)" 1>&2
    echo "    --prep-3.3     Prepare 3.3 config.xml schema for 4.x compliance" 1>&2
    echo "    --pre-process  Perform data truncation after update and generate table dump file for on-site upgrade (later)" 1>&2
    echo "    --skip-db      Skip upgrading the database and go directly to config upgrade (unsupported for 3.3 as migration is needed)" 1>&2
    echo "    --help         Display this help message and exit" 1>&2
}

# Error function
errout()
{
    echo ${1+"$@"}
    exit 1
}

# Bail on errors
set -e

# Parse command line
while [ ${#} -gt 0 ]; do
    case "$1" in
        -h)
            shift
            [ ${#} -gt 0 ] || errout "\`-h' flag requires an argument"
            DB_HOST="$1"
            shift
            ;;
        -u)
            shift
            [ ${#} -gt 0 ] || errout "\`-u' flag requires an argument"
            DB_USER="$1"
            shift
            ;;
        -p)
            shift
            [ ${#} -gt 0 ] || errout "\`-p' flag requires an argument"
            DB_PASS="$1"
            shift
            ;;
        -c)
            shift
            [ ${#} -gt 0 ] || errout "\`-c' flag requires an argument"
            CONFIG_XML="$1"
            shift
            ;;
        --prep-3.3)
            PREP_3_3="prep-3.3"
            shift
            ;;
        --pre-process)
            PRE_PROCESS="pre-process"
            shift
            ;;
        --skip-db)
            SKIP_DB="skip-db-upgrade"
            shift
            ;;
        -h|--help)
            usage
            exit
            ;;
        --)
            shift
            break
            ;;
        -*)
            errout "unrecognized flag \`${1}'"
            ;;
        *)
            break
            ;;
    esac
done
case "${#}" in
    1)
        DB_DATABASE="$1"
        shift
        ;;
    *)
        usage
        exit 1
        ;;
esac

#resolve config path
CONFIG_XML=$(readlink -f $CONFIG_XML)

# Build classpath
CLASSPATH="${CLASSDIR}"
CLASSPATH="${CLASSPATH}`find ${LIBDIR} -type f -print0 | xargs -0 -n 1 printf ':%s'`"

# Build URL
URL="jdbc:mysql://${DB_HOST}/${DB_DATABASE}?user=${DB_USER}&password=${DB_PASS}&${URL_PARAMS}"

# Run it
exec java -classpath "${CLASSPATH}" "${MAINCLASS}" "${URL}" "${CONFIG_XML}" ${PREP_3_3} ${PRE_PROCESS} ${SKIP_DB}
