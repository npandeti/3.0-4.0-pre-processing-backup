/*
 * Copyright (C) 2016 Motorola Solutions, Inc. All rights reserved.
 *
 * $Id: E911ConfigMigrator.java 90267 2019-04-17 20:28:55Z cduplichien $
 */

package com.clj2.business.config;

import com.clj2.business.lib.callstate.SIPNames;
import com.clj2.business.model.HasTimer;
import com.clj2.business.model.TrunkType;
import com.clj2.business.model.UpgradedConfigObject;
import com.clj2.business.util.HasUUID;
import com.clj2.business.util.MigrationUtils;
import com.clj2.business.util.StringUtils;
import com.clj2.business.util.WorkaroundJibxMarshaller;
import com.clj2.business.validation.Validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBasedTable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintViolation;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.dellroad.stuff.pobj.PersistentObject;
import org.dellroad.stuff.pobj.PersistentObjectDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

public final class E911ConfigMigrator {

    private static final E911ConfigMigrator INSTANCE = new E911ConfigMigrator();
    private static final Logger LOGGER = LoggerFactory.getLogger(E911ConfigMigrator.class);

    private Set<String> validDispatchGroups = new HashSet<>();

    private HashBasedTable<UUID, String, SoftphoneButton> oldUUIDToSoftphoneButtonMap = HashBasedTable.create();
    private HashBasedTable<UUID, String, DirectoryButton> oldUUIDToDirectoryButtonMap = HashBasedTable.create();

    private E911ConfigMigrator() {
    }

    public static E911ConfigMigrator getInstance() {
        return INSTANCE;
    }

    public void migrateIfNecessary(org.springframework.core.io.Resource defaultXML,
      org.springframework.core.io.Resource configXML, PersistentObjectDelegate<E911Config> schemaDelegate) throws Exception {
        LOGGER.info("---Starting CONFIG MIGRATION---");
        final String driverClass = E911Properties.getRequiredProperty("e911.db.driverClass");
        final String jdbcUrl = E911Properties.getRequiredProperty("e911.db.jdbcUrl");
        final String user = E911Properties.getRequiredProperty("e911.db.user");
        final String pass = E911Properties.getRequiredProperty("e911.db.pass");
        Class.forName(driverClass);
        try (Connection con = DriverManager.getConnection(jdbcUrl, user, pass)) {
            con.setAutoCommit(false);
            try (Statement stmt = con.createStatement()) {
                ResultSet rs = stmt.executeQuery("SELECT count(1) FROM UpgradedConfigObject");
                if (rs.next()) {
                    int count = rs.getInt(1);
                    if (count == 0) {
                        LOGGER.info("No UpgradedConfigObjects found; site must already be on 4.x");
                        return;
                    }
                    LOGGER.info("UpgradedConfigObjects found; beginning database migration process");

                    rs.close();

                    // Create JiBX marshaller that works around http://jira.codehaus.org/browse/JIBX-492
                    final WorkaroundJibxMarshaller jibxMarshaller = new WorkaroundJibxMarshaller();
                    jibxMarshaller.setTargetClass(E911Config.class);
                    jibxMarshaller.setBindingName("binding");
                    jibxMarshaller.setEncoding("UTF-8");
                    jibxMarshaller.setIndent(4);
                    jibxMarshaller.afterPropertiesSet();

                    LOGGER.info("Unmarshalling config.xml via JiBX bindings");
                    E911Config config = (E911Config)jibxMarshaller.unmarshal(new StreamSource(defaultXML.getInputStream()));
                    LOGGER.info("E911Config is ready; time to start populating it with database objects");
                    E911Init init = new E911Init();
                    init.setConfigMaster(config.getE911Init().getConfigMaster());
                    init.setReferenceData(new ReferenceData());
                    init.setAdminPhones(config.getE911Init().getAdminPhones());
                    init.setPositions(config.getE911Init().getPositions());
                    init.setAIUs(config.getE911Init().getAIUs());
                    init.setDialSetups(config.getE911Init().getDialSetups());
                    config.setE911Init(init);

                    validDispatchGroups
                      .addAll(CollectionUtils.collect(config.getDispatchGroups(), TransformerUtils.invokerTransformer("getName")));
                    LOGGER.debug("VALID DISPATCH GROUPS: " + validDispatchGroups.toString());

                    final ObjectMapper mapper = new ObjectMapper();
                    for (UpgradedConfigObject.UpgradeOrder order : UpgradedConfigObject.UpgradeOrder.values()) {
                        rs = stmt.executeQuery("select id, jsonBlob from UpgradedConfigObject where upgradeOrder = '" + order.name()
                          + "'");
                        LOGGER.info(order.name());
                        List<Object> objectList = new ArrayList<>();
                        while (rs.next()) {
                            UUID uuid = UUID.fromString(rs.getString(1));
                            String json = rs.getString(2).replaceAll("\n", " ");

                            if (!json.equals("")) {
                                Class<?> klass = getType(order);

                                json = makeCompatibleWithCode(order, json);

                                LOGGER.debug(rs.getRow() + "-" + json);
                                Object o = mapper.readValue(json, klass);
                                if (HasUUID.class.isAssignableFrom(klass)) {
                                    ((HasUUID)o).setId(uuid);
                                }
                                objectList.add(o);
                            }
                        }
                        rs.close();
                        LOGGER.debug(String.format("%s %s object(s)", objectList.size(), order.name()));

                        if (order == UpgradedConfigObject.UpgradeOrder.TRUNK) {
                            setTrunks(config, convert(objectList, Trunk.class));
                        } else {
                            setObjects(config, order, objectList);
                        }
                    }

                    fixSoftphoneAndDirectoryUUIDs(config);
                    checkRunGroupNames(config);
                    // remove timers with no dispatch groups
                    removeUnusedTimers(config);

                    LOGGER.info("E911Config object population complete; validating...");
                    Set<ConstraintViolation<E911Config>> violations = config.validate();
                    if (violations.isEmpty()) {
                        LOGGER.info("VALID !!");

                        logSpELExpressions(config);

                        int deleted = stmt.executeUpdate("delete from UpgradedConfigObject");
                        if (deleted == count) {
                            con.commit();

                            Path tmpDir = Paths.get(System.getProperty("java.io.tmpdir"));
                            Path migratedConfigFilePath = Files.createTempFile(tmpDir, "migrated-config.xml", null);
                            LOGGER.info("Writing migrated config.xml to \"" + migratedConfigFilePath + "\"");
                            PersistentObject.write(config, schemaDelegate, migratedConfigFilePath.toFile());

                            LOGGER.info("Performing post-migration config file modifications");
                            ConfigPrep configPrep = new ConfigPrep(defaultXML, configXML);
                            configPrep.postMigrationAssembly(new FileSystemResource(migratedConfigFilePath.toFile()));

                            LOGGER.info("Creating symbolic link to config.xml (if needed)");
                            Path configFilePath = Paths.get(configXML.getURI());
                            Path configLinkPath = Paths.get(defaultXML.getFile().getParent() + "/config.xml");
                            if (!Files.isSymbolicLink(configLinkPath) && !configFilePath.equals(configLinkPath)) {
                                Files.createSymbolicLink(configLinkPath, configFilePath);
                            }

                            LOGGER.info("---CONFIG MIGRATION - COMPLETE---");
                        } else {
                            throw new RuntimeException("Expected to delete " + count + " records but only deleted " + deleted);
                        }
                    } else {
                        StringBuilder builder = new StringBuilder();
                        builder.append("Found ").append(violations.size()).append(" config violation(s) after migration!");
                        for (ConstraintViolation<E911Config> violation : violations) {
                            String path = violation.getPropertyPath().toString();
                            if (path.length() == 0) {
                                path = "top level";
                            }
                            builder.append(path).append(": ").append(violation.getMessage()).append("\n");
                        }
                        LOGGER.error(builder.toString());
                        throw new RuntimeException(builder.toString());
                    }
                }
            }
        }
    }

    /**
     * There may be SpEL expressions used throughout the config that need to be updated to reflect new, 4.0 field names (e.g. id ->
     * displayId). These expressions are free-form text that our conventional validation rules can't accurately validate (i.e. we
     * can't know what user's intentions are).
     * <p>
     * This method outputs those expressions to the console, with a request to review them manually.
     *
     * @param config
     */
    private void logSpELExpressions(E911Config config) {
        LOGGER.info("************************************ SpEL EXPRESSIONS *****************************************");
        LOGGER.info("***************************** PLEASE REVIEW FOR CORRECTNESS ***********************************");

        // map related
        if (config.getMapConfig() != null) {
            for (QueryLayer queryLayer : config.getMapConfig().getQueryLayers()) {
                if (queryLayer.getHoverTemplate() != null) {
                    LOGGER.info("MapConfig.QueryLayer.HoverTemplate:  " + queryLayer.getHoverTemplate().getExpressionString());
                }
            }
            if (config.getMapConfig().getHoverQuery() != null && config.getMapConfig().getHoverQuery().getESNFormat() != null) {
                LOGGER
                  .info(("MapConfig.HoverQuery.ESNFormat:  " +
                    config.getMapConfig().getHoverQuery().getESNFormat().getExpressionString()));
            }
            if (config.getMapConfig().getESNFormat() != null) {
                LOGGER.info(("MapConfig.ESNFormat:  " + config.getMapConfig().getESNFormat().getExpressionString()));
            }
        }

        // site related
        LOGGER.info("SiteConfig.CaseNumberExpression:  " + config.getSiteConfig().getCaseNumberExpression().getExpressionString());
        for (Agency agency : config.getReferenceData().getAgencies()) {
            LOGGER.info("ReferenceData.Agency.CaseNumberExpression:  " + agency.getCaseNumberExpressionString());
        }

        // ALI related
        for (ALISerialConfig aliSerialConfig : config.getALISerialConfigs()) {
            LOGGER.info("ALISerialConfig.ALISteerFilter:  " + aliSerialConfig.getALISteerFilter().getExpressionString());
        }
        for (ALIConfig aliConfig : config.getALIConfigs()) {
            for (AutoRebid autoRebid : aliConfig.getAutoRebids()) {
                LOGGER.info("ALIConfig.AutoRebid.Match:  " + autoRebid.getMatch().getExpressionString());
            }
        }

        // GUI related
        if (config.getCallConfig() != null && config.getCallConfig().getCallDisplays() != null) {
            for (CallDisplay callDisplay : config.getCallConfig().getCallDisplays()) {
                LOGGER.info("CallConfig.CallDisplay.Match:  " + callDisplay.getMatch().getExpressionString());
                LOGGER.info("CallConfig.CallDisplay.Display:  " + callDisplay.getDisplay().getExpressionString());
            }
        }

        // notification templates
        for (NotificationTemplate template : config.getReferenceData().getNotificationTemplates()) {
            LOGGER.info("ReferenceData.NotificationTemplate.Subject:  " + template.getSubjectString());
            LOGGER.info("ReferenceData.NotificationTemplate.Message:  " + template.getMessageString());
        }

        LOGGER.info("***********************************************************************************************");
    }

    /**
     * Prepares JSON config objects to make them compatible with their corresponding Java class structure, where fields have been
     * modified since the JSON was generated.
     *
     * @param order
     * @param json
     * @return
     */
    private String makeCompatibleWithCode(UpgradedConfigObject.UpgradeOrder order, String json) {
        if (UpgradedConfigObject.UpgradeOrder.SYSTEM_USER.equals(order)) {
            // correct user roles
            json = json.replaceAll("SUPERUSERS", "USERS"); // SUPERUSERS is defunct; go with USERS to keep things moving
            json = json.replaceAll("WEBACCESSORY", "DECISION_STATION");
        } else if (UpgradedConfigObject.UpgradeOrder.DIRECTORY_BUTTON.equals(order)
          || UpgradedConfigObject.UpgradeOrder.SOFTPHONE_BUTTON.equals(order)
          || UpgradedConfigObject.UpgradeOrder.DIRECTORY_ENTRY.equals(order)) {
            json = json.replaceAll("preferredNumber", "preferredContact");
        } else if (UpgradedConfigObject.UpgradeOrder.AUTODIAL_CONTEXT.equals(order)) {
            json = json.replaceAll("phoneNumbers", "contacts");
            json = json.replaceAll("defaultPhoneNumber", "defaultContact");
        }
        return json;
    }

    private void setObjects(E911Config config, UpgradedConfigObject.UpgradeOrder order, List<Object> objectList) {
        switch (order) {
        case UNIT_TYPE_GROUP:
            List<UnitTypeGroup> typeGroups = convert(objectList, UnitTypeGroup.class);
            for (UnitTypeGroup group : typeGroups) {
                group.setLabel(MigrationUtils.trimWhitespace(group.getLabel()));
                MigrationUtils.checkAndSetIcon(group);
            }
            config.getReferenceData().getUnitData().setTypeGroups(typeGroups);
            break;
        case UNIT_TYPE:
            List<UnitType> types = convert(objectList, UnitType.class);
            for (UnitType type : types) {
                type.setLabel(MigrationUtils.trimWhitespace(type.getLabel()));
            }
            config.getReferenceData().getUnitData().setTypes(types);
            break;
        case INCIDENT_PRIORITY:
            List<IncidentPriority> incidentPriorities = convert(objectList, IncidentPriority.class);
            for (IncidentPriority priority : incidentPriorities) {
                MigrationUtils.checkAndGenerateAbbreviationAndPosition(priority, incidentPriorities);
            }
            config.getReferenceData().getIncidentData().setPriorities(incidentPriorities);
            break;
        case INCIDENT_TYPE:
            List<IncidentType> incidentTypes = convert(objectList, IncidentType.class);
            for (IncidentType incidentType : incidentTypes) {
                MigrationUtils.checkAndAddToSounds(incidentType.getTimer(), config.getE911Service().getSounds());
                incidentType.setLabel(MigrationUtils.trimWhitespace(incidentType.getLabel()));
            }
            config.getReferenceData().getIncidentData().setTypes(incidentTypes);
            break;
        case INCIDENT_DISPOSITION:
            config.getReferenceData().getIncidentData().setDispositions(convert(objectList, IncidentDisposition.class));
            break;
        case INCIDENT_SUBTYPE:
            List<IncidentSubtype> subTypes = convert(objectList, IncidentSubtype.class);
            for (IncidentSubtype subtype : subTypes) {
                subtype.setLabel(MigrationUtils.trimWhitespace(subtype.getLabel()));
            }
            config.getReferenceData().getIncidentData().setSubtypes(subTypes);
            break;
        case INCIDENT_STATUS:
            List<IncidentStatus> incidentStatuses = convert(objectList, IncidentStatus.class);
            for (IncidentStatus status : incidentStatuses) {
                MigrationUtils.checkAndGenerateAbbreviation(status, incidentStatuses);
            }
            config.getReferenceData().getIncidentData().setStatuses(incidentStatuses);
            break;
        case UNIT_STATUS:
            List<UnitStatus> unitStatuses = convert(objectList, UnitStatus.class);
            for (UnitStatus status : unitStatuses) {
                if (status.isCanonicalUnitStatus()) {
                    status.setRequired(true); // canonical statuses should all be required
                } else {
                    // all statuses must have a canonical status tied to them; best I know to do is set the following...
                    status.setCanonicalUnitStatusUUID(UUIDConstant.UnitStatusUUID.OUT_OF_SERVICE.getUUID());
                }
                MigrationUtils.checkAndGenerateAbbreviation(status, unitStatuses);
                MigrationUtils.checkAndAddToSounds(status.getTimer(), config.getE911Service().getSounds());
            }
            config.getReferenceData().getUnitData().setStatuses(unitStatuses);
            break;
        case DISPATCH_CONFIG:
            config.getE911Init().setDispatchConfig((DispatchConfig)objectList.get(0));
            break;
        case NOTIFICATION_TEMPLATE:
            config.getReferenceData().setNotificationTemplates(convert(objectList, NotificationTemplate.class));
            break;
        case CREATE_INCIDENT_NOTIFICATION:
            config.getReferenceData().getNotifications().addAll(convert(objectList, CreateIncidentNotification.class));
            break;
        case DISPATCH_UNIT_NOTIFICATION:
            List<DispatchUnitNotification> notifications = convert(objectList, DispatchUnitNotification.class);
            Iterator<DispatchUnitNotification> iter = notifications.iterator();
            while (iter.hasNext()) {
                DispatchUnitNotification notification = iter.next();
                if (notification.getUnitUUID() != null && notification.getAgencyUUID() != null) {
                    notification.setAgencyUUID(null); // can't have both
                }
            }
            config.getReferenceData().getNotifications().addAll(notifications);
            break;
        case DISPATCH_RUN_GROUP_NOTIFICATION:
            config.getReferenceData().getNotifications().addAll(convert(objectList, DispatchRunGroupNotification.class));
            break;
        case REQUEST_NOTIFICATION:
            config.getReferenceData().getNotifications().addAll(convert(objectList, RequestNotification.class));
            break;
        case PREDEFINED_MESSAGE:
            config.getReferenceData().setPredefinedMessages(convert(objectList, PredefinedMessage.class));
            break;
        case COMMON_PLACE_NAME:
            List<CommonPlaceName> commonPlaceNames = convert(objectList, CommonPlaceName.class);
            for (CommonPlaceName commonPlaceName : commonPlaceNames) {
                commonPlaceName.setName(MigrationUtils.trimWhitespace(commonPlaceName.getName()));
            }
            config.getReferenceData().setCommonPlaceNames(convert(objectList, CommonPlaceName.class));
            break;
        case TANDEM_GROUP:
            List<TandemGroup> tandemGroups = convert(objectList, TandemGroup.class);
            for (TandemGroup group : tandemGroups) {
                MigrationUtils.checkAndSetDialDelay(group);
            }
            if (config.getAsteriskConfig() != null) {
                config.getAsteriskConfig().setTandemGroups(tandemGroups);
            }
            break;
        case TANDEM_TRANSFER_CODE:
            config.getReferenceData().setTandemTransferCodes(convert(objectList, TandemTransferCode.class));
            break;
        case REQUEST_PROVIDER:
            config.getReferenceData().getRequests().setRequestProviders(convert(objectList, RequestProvider.class));
            break;
        case SOFTPHONE_BUTTON:
            List<SoftphoneButton> softphoneButtons = convert(objectList, SoftphoneButton.class);
            for (SoftphoneButton softphoneButton : softphoneButtons) {
                softphoneButton.setDescription(MigrationUtils.trimWhitespace(softphoneButton.getDescription()));
            }
            oldUUIDToSoftphoneButtonMap =
              MigrationUtils.cleanAndCopyForDispatchGroups(validDispatchGroups, softphoneButtons, SoftphoneButton.class);
            config.getReferenceData().setSoftphoneButtons(new ArrayList<>(oldUUIDToSoftphoneButtonMap.values()));
            break;
        case SOFTPHONE_BUTTON_ORDERING:
            List<SoftphoneButtonOrdering> softphoneButtonOrderings = convert(objectList, SoftphoneButtonOrdering.class);

            Map<String, SoftphoneButtonOrdering> softphoneButtonOrderingMap = new HashMap<>();
            for (SoftphoneButtonOrdering softphoneButtonOrdering : softphoneButtonOrderings) {

                if (softphoneButtonOrdering.getDispatchGroupNames().size() > 1)
                    continue;

                Set<String> dgNames = MigrationUtils.cleanDispatchGroups(
                  validDispatchGroups, softphoneButtonOrdering.getDispatchGroupNames(), false);

                if (dgNames.size() != 1)
                    continue;

                softphoneButtonOrdering.setUUID(UUID.randomUUID());
                softphoneButtonOrderingMap.putIfAbsent(softphoneButtonOrdering.getDispatchGroupName(), softphoneButtonOrdering);
            }

            config.getReferenceData().setSoftphoneButtonOrderings(new ArrayList<>(softphoneButtonOrderingMap.values()));
            break;
        case DIRECTORY_ENTRY_TYPE:
            List<DirectoryEntryType> directoryEntryTypes = convert(objectList, DirectoryEntryType.class);
            for (DirectoryEntryType type : directoryEntryTypes) {
                type.setLabel(MigrationUtils.trimWhitespace(type.getLabel()));
            }
            config.getReferenceData().setDirectoryEntryTypes(directoryEntryTypes);
            break;
        case DIRECTORY_BUTTON:
            List<DirectoryButton> directoryButtons = convert(objectList, DirectoryButton.class);
            for (DirectoryButton directoryButton : directoryButtons) {
                directoryButton.setDescription(MigrationUtils.trimWhitespace(directoryButton.getDescription()));
            }
            oldUUIDToDirectoryButtonMap =
              MigrationUtils.cleanAndCopyForDispatchGroups(validDispatchGroups, directoryButtons, DirectoryButton.class);
            config.getReferenceData().setDirectoryButtons(new ArrayList<>(oldUUIDToDirectoryButtonMap.values()));
            break;
        case DIRECTORY_BUTTON_ORDERING:
            List<DirectoryButtonOrdering> directoryButtonOrderings = convert(objectList, DirectoryButtonOrdering.class);

            HashMap<String, DirectoryButtonOrdering> nullOrder = new HashMap<>();
            HashMap<String, HashMap<UUID, DirectoryButtonOrdering>> dgToDirectoryTypeToDirectoryButtonOrderingMap =
              new HashMap<>();
            for (DirectoryButtonOrdering directoryButtonOrdering : directoryButtonOrderings) {

                if (directoryButtonOrdering.getDispatchGroupNames().size() > 1)
                    continue;

                Set<String> dgNames = MigrationUtils.cleanDispatchGroups(
                  validDispatchGroups, directoryButtonOrdering.getDispatchGroupNames(), false);

                if (dgNames.size() != 1)
                    continue;

                if (directoryButtonOrdering.getDirectoryEntryTypeUUID() == null) {
                    nullOrder.putIfAbsent(directoryButtonOrdering.getDispatchGroupName(), directoryButtonOrdering);
                } else if (dgToDirectoryTypeToDirectoryButtonOrderingMap
                  .get(directoryButtonOrdering.getDispatchGroupName()) == null) {
                    HashMap<UUID, DirectoryButtonOrdering> tempMap = new HashMap<>();
                    directoryButtonOrdering.setUUID(UUID.randomUUID());
                    tempMap.put(directoryButtonOrdering.getDirectoryEntryTypeUUID(), directoryButtonOrdering);
                    dgToDirectoryTypeToDirectoryButtonOrderingMap.put(directoryButtonOrdering.getDispatchGroupName(), tempMap);
                } else if (dgToDirectoryTypeToDirectoryButtonOrderingMap.get(directoryButtonOrdering.getDispatchGroupName())
                  .get(directoryButtonOrdering.getDirectoryEntryTypeUUID()) == null) {
                    HashMap<UUID, DirectoryButtonOrdering> typeMap = dgToDirectoryTypeToDirectoryButtonOrderingMap
                      .get(directoryButtonOrdering.getDispatchGroupName());
                    directoryButtonOrdering.setUUID(UUID.randomUUID());
                    typeMap.put(directoryButtonOrdering.getDirectoryEntryTypeUUID(), directoryButtonOrdering);
                } else {
                    LOGGER.warn("E911ConfigMigrator: Multiple directory button orderings for same dg and type combo.");
                }
            }

            List<DirectoryButtonOrdering> directoryButtonOrderingListToReturn =
              dgToDirectoryTypeToDirectoryButtonOrderingMap
                .values()
                .stream()
                .flatMap(x -> x.values().stream())
                .collect(Collectors.toList());
            if (nullOrder.size() > 0)
                directoryButtonOrderingListToReturn.addAll(nullOrder.values());
            config.getReferenceData().setDirectoryButtonOrderings(directoryButtonOrderingListToReturn);
            break;
        case DIRECTORY_ENTRY:
            List<DirectoryEntry> directoryEntries = convert(objectList, DirectoryEntry.class);
            for (DirectoryEntry entry : directoryEntries) {
                entry.setDescription(MigrationUtils.trimWhitespace(entry.getDescription()));
            }
            config.getReferenceData().setDirectoryEntries(
              new ArrayList<>(
                MigrationUtils.cleanAndCopyForDispatchGroups(validDispatchGroups, directoryEntries, DirectoryEntry.class)
                  .values()));
            break;
        case AUTODIAL_CONTEXT:
            List<AutodialContext> autodialContexts = convert(objectList, AutodialContext.class);
            for (AutodialContext context : autodialContexts) {
                context.setDescription(MigrationUtils.trimWhitespace(context.getDescription()));
                context.setDispatchGroupName(null); // default to global group (needed to pass pobj validation)
            }
            config.getReferenceData().setAutodialContexts(autodialContexts);
            break;
        case AGENCY:
            config.getReferenceData().setAgencies(convert(objectList, Agency.class));
            break;
        case UNIT:
            List<Unit> units = convert(objectList, Unit.class);
            for (Unit unit : units) {
                unit.setDescription(MigrationUtils.trimWhitespace(unit.getDescription()));
                if (StringUtils.isEmpty(unit.getDescription())) {
                    unit.setDescription(unit.getName());
                }
                unit.setDispatchGroupNames(
                  MigrationUtils.cleanDispatchGroups(validDispatchGroups, unit.getDispatchGroupNames(), false));
            }
            config.getReferenceData().getUnitData().setUnits(units);
            break;
        case RUN_GROUP:
            List<RunGroup> runGroups = convert(objectList, RunGroup.class);
            for (RunGroup runGroup : runGroups) {
                runGroup.setName(MigrationUtils.trimWhitespace(runGroup.getName()));
                runGroup.setName(runGroup.getName().replaceAll(" ", "-")); // run groups can't have ANY whitespace
                runGroup.setDescription(MigrationUtils.trimWhitespace(runGroup.getDescription().trim()));
            }
            config.getReferenceData().setRunGroups(runGroups);
            break;
        case REQUEST_CATEGORY:
            config.getReferenceData().getRequests().setRequestCategories(convert(objectList, RequestCategory.class));
            break;
        case AGENCY_USER:
            List<AgencyUser> users = convert(objectList, AgencyUser.class);
            for (AgencyUser user : users) {
                user.setDispatchGroupNames(
                  MigrationUtils.cleanDispatchGroups(validDispatchGroups, user.getDispatchGroupNames(), false));
            }
            config.getE911Init().setAgencyUsers(users);
            break;
        case SYSTEM_USER:
            List<SystemUser> systemUsers = convert(objectList, SystemUser.class);
            for (SystemUser user : systemUsers) {
                MigrationUtils.cleanUsername(user);
                user.setDispatchGroupNames(
                  MigrationUtils.cleanDispatchGroups(validDispatchGroups, user.getDispatchGroupNames(), false));
            }
            config.getE911Init().setUsers(systemUsers);

            // take this opportunity to cleanup all positions' default users
            for (Position position : config.getPositions()) {
                MigrationUtils.cleanUsername(position.getDefaultUser());
            }
            break;
        case FACT:
            List<Fact> facts = convert(objectList, Fact.class);
            for (Fact fact : facts) {
                fact.setDescription(MigrationUtils.trimWhitespace(fact.getDescription()));
            }
            config.getReferenceData().setFacts(facts);
            break;
        case MAP_VIEW_CONFIG:
            config.getReferenceData().setMapViewConfigs(convert(objectList, MapViewConfig.class));
            break;
        default:
            throw new RuntimeException("unknown upgrade ordering `" + order + "'");
        }
    }

    // handles trunks and sip peers
    private void setTrunks(E911Config config, List<Trunk> trunks) {
        for (Trunk trunk : trunks) {
            String name = trunk.getCarrierID().replaceAll("^trunk-gateway-", "");
            if (name.startsWith("sip-peer")) {
                int peerIndex = Integer.parseInt(name.substring(name.lastIndexOf('-') + 1)) - 1;
                SIPPeer sipPeer = config.getAsteriskConfig().getSIPPeers().get(peerIndex);
                assert sipPeer != null;
                assert sipPeer.getTrunkID() == trunk.getTrunkID();
                assert routeExists(sipPeer, trunk);
                sipPeer.setId(trunk.getId());
                sipPeer.setTandemGroupUUID(trunk.getTandemGroupUUID());
            } else {
                String[] tokens = name.split(SIPNames.SIP_NAME_DELIMITER);
                assert tokens.length == 3;
                if (tokens.length == 3) {
                    TrunkType type = trunk.getTrunkType();
                    int index = Integer.parseInt(tokens[1]) - 1;
                    int port = Integer.parseInt(tokens[2]);
                    if (index > -1 && index < config.getAsteriskConfig().getGateways().size()) {
                        for (Trunk gwTrunk : safeTrunks(config.getAsteriskConfig().getGateways().get(index).getTrunks())) {
                            if (gwTrunk.getPort() == port) {
                                assert gwTrunk.getTrunkType() == type;
                                assert gwTrunk.getTrunkID() == trunk.getTrunkID();
                                gwTrunk.setId(trunk.getId());
                                gwTrunk.setTandemGroupUUID(trunk.getTandemGroupUUID());
                            }
                        }
                    }
                }
            }
        }
    }

    private void fixSoftphoneAndDirectoryUUIDs(E911Config config) {

        for (SoftphoneButtonOrdering softphoneButtonOrdering : config.getReferenceData().getSoftphoneButtonOrderings()) {
            LinkedHashMap<UUID, Integer> sortedOrdering = softphoneButtonOrdering
              .getOrdering()
              .entrySet()
              .stream()
              .filter(e -> oldUUIDToSoftphoneButtonMap.contains(e.getKey(), softphoneButtonOrdering.getDispatchGroupName()))
              .sorted(Comparator.comparingInt(Map.Entry::getValue))
              .collect(Collectors.toMap(
                e -> oldUUIDToSoftphoneButtonMap.get(e.getKey(), softphoneButtonOrdering.getDispatchGroupName()).getId(),
                Map.Entry::getValue,
                (a, b) -> a,
                LinkedHashMap::new));
            softphoneButtonOrdering.setOrdering(sortedOrdering);
        }

        for (DirectoryButtonOrdering directoryButtonOrdering : config.getReferenceData().getDirectoryButtonOrderings()) {
            LinkedHashMap<UUID, Integer> sortedOrdering = directoryButtonOrdering
              .getOrdering()
              .entrySet()
              .stream()
              .filter(e -> oldUUIDToDirectoryButtonMap.contains(e.getKey(), directoryButtonOrdering.getDispatchGroupName()))
              .sorted(Comparator.comparingInt(Map.Entry::getValue))
              .collect(Collectors.toMap(
                e -> oldUUIDToDirectoryButtonMap.get(e.getKey(), directoryButtonOrdering.getDispatchGroupName()).getId(),
                Map.Entry::getValue,
                (a, b) -> a,
                LinkedHashMap::new));
            directoryButtonOrdering.setOrdering(sortedOrdering);
        }
    }

    private void checkRunGroupNames(E911Config config) {
        config.getReferenceData().getRunGroups()
          .stream()
          .filter(runGroup -> config.getReferenceData().getUnitData().getUnits()
            .stream()
            .anyMatch(unit -> unit.getName().equals(runGroup.getName())))
          .forEach(runGroup -> runGroup.setName(runGroup.getName() + "-run-group"));
    }

    private void removeUnusedTimers(E911Config config) {
        Stream<UnitStatus> unitStatusesStream = config.getReferenceData().getUnitData().getStatuses().stream();
        Stream<IncidentType> incidentTypesStream = config.getReferenceData().getIncidentData().getTypes().stream();
        Stream<HasTimer> streamsWithTimers = Stream.concat(unitStatusesStream, incidentTypesStream);

        streamsWithTimers
          .filter(hasTimer -> hasTimer.getTimer() != null && hasTimer.getTimer().getDispatchGroupNames().size() == 0)
          .forEach(hasTimer -> hasTimer.setTimer(null));
    }

    private List<Trunk> safeTrunks(List<Trunk> trunks) {
        return trunks == null ? Collections.<Trunk>emptyList() : trunks;
    }

    private boolean routeExists(SIPPeer sipPeer, Trunk trunk) {
        for (InboundRoute route : sipPeer.getInboundRoutes()) {
            if (route.getTrunkType() == trunk.getTrunkType()
              && Validator.equals(route.getDispatchGroup(), trunk.getDispatchGroup())) {
                return true;
            }
        }
        return false;
    }

    private <T> List<T> convert(List<Object> objectList, Class<T> klass) {
        final List<T> list = new ArrayList<>(objectList.size());
        for (Object o : objectList) {
            list.add(klass.cast(o));
        }
        return list;
    }

    private Class<?> getType(UpgradedConfigObject.UpgradeOrder upgradeOrder) throws ClassNotFoundException {
        String[] nameParts = upgradeOrder.name().split("_");
        StringBuilder builder = new StringBuilder("com.clj2.business.config.");
        for (String part : nameParts) {
            if (part != null) {
                builder.append(part.charAt(0));
                builder.append(part.substring(1).toLowerCase());
            }
        }
        return Class.forName(builder.toString());
    }
}
